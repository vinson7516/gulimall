package com.atguigu.gulimall.member.service;

import com.atguigu.common.utils.PageUtils;
import com.baomidou.mybatisplus.extension.service.IService;
import com.atguigu.gulimall.member.entity.IntegrationChangeHistoryEntity;

import java.util.Map;

/**
 * 积分变化历史记录
 *
 * @author xdli
 * @email xdli@sec.ac.cn
 * @date 2022-12-06 19:52:25
 */
public interface IntegrationChangeHistoryService extends IService<IntegrationChangeHistoryEntity> {


    PageUtils queryPage(Map<String, Object> params);
}

