package com.atguigu.gulimall.member.service;

import com.atguigu.common.utils.PageUtils;
import com.baomidou.mybatisplus.extension.service.IService;
import com.atguigu.gulimall.member.entity.MemberLevelEntity;

import java.util.Map;

/**
 * 会员等级
 *
 * @author xdli
 * @email xdli@sec.ac.cn
 * @date 2022-12-06 19:52:25
 */
public interface MemberLevelService extends IService<MemberLevelEntity> {


    PageUtils queryPage(Map<String, Object> params);
}

